package xyz.dom133.polskakorona.Addons;

public class Constants {

    private Constants() {}

    //URL
    public static final String URL_WEB = "api.dom133.pl";
    public static final String URL_CORRONA_JSON = "http://"+ URL_WEB +"/get_corona_json";

    //Preferences
    public static final String PREF_INFECTED = "infected_num";
    public static final String PREF_INFECTED_UP = "infected_up_num";
    public static final String PREF_INFECTED_HISCORE = "infected_hiscore_num";
    public static final String PREF_KILLED = "killed_num";
    public static final String PREF_KILLED_UP = "killed_up_num";
    public static final String PREF_KILLED_HISCORE = "killed_hiscore_num";
    public static final String PREF_UPDATE_TIME = "update_time_num";

    //App
    public static final String PACKAGE_NAME = "xyz.dom133.polskakorona";
}
