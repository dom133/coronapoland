package xyz.dom133.polskakorona.Addons;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceHelper {

    private PreferenceHelper() {}

    /**
     * Function get number of infected
     * @param context ApplicationContext
     * @return String
     */
    public static String getInfected(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(Constants.PREF_INFECTED, "0");
    }

    /**
     * Function set number of infected
     * @param context ApplicationContext
     * @param value String
     */
    public static void setInfected(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(Constants.PREF_INFECTED, value).apply();
    }

    /**
     * Function get number of infected from last update
     * @param context ApplicationContext
     * @return String
     */
    public static String getInfectedUp(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(Constants.PREF_INFECTED_UP, "0");
    }

    /**
     * Function set number of infected from last update
     * @param context ApplicationContext
     * @param value String
     */
    public static void setInfectedUp(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(Constants.PREF_INFECTED_UP, value).apply();
    }

    /**
     * Function get number of hiscore infected
     * @param context ApplicationContext
     * @return String
     */
    public static String getInfectedHiscore(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(Constants.PREF_INFECTED_HISCORE, "0");
    }

    /**
     * Function set number of hiscore infected
     * @param context ApplicationContext
     * @param value String
     */
    public static void setInfectedHiscore(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(Constants.PREF_INFECTED_HISCORE, value).apply();
    }

    /**
     * Function get number of killed
     * @param context ApplicationContext
     * @return String
     */
    public static String getKilled(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(Constants.PREF_KILLED, "0");
    }

    /**
     * Function set number of killed
     * @param context ApplicationContext
     * @param value String
     */
    public static void setKilled(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(Constants.PREF_KILLED, value).apply();
    }

    /**
     * Function get number of killed from last update
     * @param context ApplicationContext
     * @return String
     */
    public static String getKilledUp(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(Constants.PREF_KILLED_UP, "0");
    }

    /**
     * Function set number of killed from last update
     * @param context ApplicationContext
     * @param value String
     */
    public static void setKilledUp(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(Constants.PREF_KILLED_UP, value).apply();
    }

    /**
     * Function get number of hiscore killed
     * @param context ApplicationContext
     * @return String
     */
    public static String getKilledHiscore(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(Constants.PREF_KILLED_HISCORE, "0");
    }

    /**
     * Function set number of hiscore killed
     * @param context ApplicationContext
     * @param value String
     */
    public static void setKilledHiscore(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(Constants.PREF_KILLED_HISCORE, value).apply();
    }

    /**
     * Function get last update time
     * @param context ApplicationContext
     * @return String
     */
    public static String getUpdateTime(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(Constants.PREF_UPDATE_TIME, "00:00:00");
    }

    /**
     * Function set last update time
     * @param context ApplicationContext
     * @param value String
     */
    public static void setUpdateTime(Context context, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(Constants.PREF_UPDATE_TIME, value).apply();
    }
}
