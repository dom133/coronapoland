package xyz.dom133.polskakorona.Addons;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;

public class CoronaWeb {
    /**
     * Function get text from web
     * @param get_url String
     * @return String
     */
    private String get_text_from_url(String get_url) {
        URL url = null;
        try {
            url = new URL(get_url);
            URLConnection con = url.openConnection();
            InputStream in = con.getInputStream();
            String encoding = con.getContentEncoding();
            encoding = encoding == null ? "UTF-8" : encoding;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[8192];
            int len = 0;
            while ((len = in.read(buf)) != -1) {
                baos.write(buf, 0, len);
            }
            return new String(baos.toByteArray(), encoding);
        } catch (Exception e) {
            return "0";
        }
    }

    /**
     * Function get json from text string
     * @param json String
     * @param name String
     * @return String
     */
    public String get_text_from_json(String json, String name) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject.getString(name);
        } catch (Exception e) {
            return "0";
        }
    }

    /**
     * Function return json string from url
     * @return String
     */
    public String get_corona_json() {
        return get_text_from_url(Constants.URL_CORRONA_JSON);
    }
}
