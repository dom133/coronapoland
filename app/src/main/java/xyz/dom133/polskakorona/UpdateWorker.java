package xyz.dom133.polskakorona;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import xyz.dom133.polskakorona.Addons.Constants;
import xyz.dom133.polskakorona.Addons.CoronaWeb;
import xyz.dom133.polskakorona.Addons.PreferenceHelper;

public class UpdateWorker extends IntentService {

    private int NOTIF_ID = 1;
    private CoronaWeb coronaWeb;
    private boolean DEBUG = false;
    private int last_update = 0;

    public UpdateWorker() {
        super(UpdateWorker.class.getSimpleName());
        coronaWeb = new CoronaWeb();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        startForeground(NOTIF_ID, getMyActivityNotification(
                String.format(getString(R.string.notify_number_of_infected),
                PreferenceHelper.getInfected(getApplicationContext()),
                PreferenceHelper.getKilled(getApplicationContext()))));

        while (true) {
            try {
                String corona_json = coronaWeb.get_corona_json();
                String update_time = coronaWeb.get_text_from_json(corona_json,
                        "update_time");
                int infected_update = Integer.parseInt(coronaWeb.get_text_from_json(corona_json,
                        "infected"));
                int infected_ls_up = Integer.parseInt(coronaWeb.get_text_from_json(corona_json,
                        "infected_up"));
                int infected_hiscore = Integer.parseInt(coronaWeb.get_text_from_json(corona_json,
                        "infected_hiscore"));
                int killed_update = Integer.parseInt(coronaWeb.get_text_from_json(corona_json,
                        "killed"));
                int killed_ls_up = Integer.parseInt(coronaWeb.get_text_from_json(corona_json,
                        "killed_up"));
                int killed_hiscore = Integer.parseInt(coronaWeb.get_text_from_json(corona_json,
                        "killed_hiscore"));

                if (last_update == 0) last_update = infected_update;

                if (last_update != infected_update && infected_update != 0) {
                    last_update = infected_update;

                    PendingIntent contentIntent = PendingIntent.getActivity(this,
                            0, new Intent(this, MainActivity.class),
                            0);

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                            "infected_01")
                            .setSmallIcon(R.drawable.ic_notification_icon)
                            .setContentTitle(getString(R.string.notify_number_new_infected_label))
                            .setContentText(String.format(
                                    getString(R.string.notify_number_new_infected),
                                    infected_ls_up, infected_update, killed_update))
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(""))
                            .setContentIntent(contentIntent)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                    notificationManager.notify(2, builder.build());
                }

                if(infected_update != 0) {
                    PreferenceHelper.setInfected(getApplicationContext(),
                            String.valueOf(infected_update));
                    PreferenceHelper.setInfectedUp(getApplicationContext(),
                            String.valueOf(infected_ls_up));
                    PreferenceHelper.setInfectedHiscore(getApplicationContext(),
                            String.valueOf(infected_hiscore));
                    PreferenceHelper.setKilled(getApplicationContext(),
                            String.valueOf(killed_update));
                    PreferenceHelper.setKilledUp(getApplicationContext(),
                            String.valueOf(killed_ls_up));
                    PreferenceHelper.setKilledHiscore(getApplicationContext(),
                            String.valueOf(killed_hiscore));
                    PreferenceHelper.setUpdateTime(getApplicationContext(),
                            update_time);

                    Intent update_service = new Intent();
                    update_service.setAction(Constants.PACKAGE_NAME);
                    update_service.putExtra("infected",
                            String.valueOf(infected_update));
                    update_service.putExtra("infected_up",
                            String.valueOf(infected_ls_up));
                    update_service.putExtra("infected_hiscore",
                            String.valueOf(infected_hiscore));
                    update_service.putExtra("killed",
                            String.valueOf(killed_update));
                    update_service.putExtra("killed_up",
                            String.valueOf(killed_ls_up));
                    update_service.putExtra("killed_hiscore",
                            String.valueOf(killed_hiscore));
                    update_service.putExtra("update_time",
                            update_time);
                    sendBroadcast(update_service);
                }

                //Needed for battery optimization
                updateNotification();

                if(DEBUG)
                    Log.d("UD", infected_update + "/" + killed_update + " | "
                            + infected_ls_up + "/" + killed_ls_up + " | "
                            + infected_hiscore + "/" + killed_hiscore + " | "
                            + last_update + " | " + update_time);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Function for notification activity
     * @param text String
     * @return Notification
     */
    private Notification getMyActivityNotification(String text){
        CharSequence title = getString(R.string.notify_number_of_infected_label);
        PendingIntent contentIntent = PendingIntent.getActivity(this,
                0, new Intent(this, MainActivity.class), 0);

        return new NotificationCompat.Builder(this, "infected_02")
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(title)
                .setContentText(text)
                .setOngoing(true)
                .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(""))
                .build();
    }

    /**
     * This is the method that can be called to update the Notification
     */
    private void updateNotification() {
        String text = String.format(getString(R.string.notify_number_of_infected),
                PreferenceHelper.getInfected(getApplicationContext()),
                PreferenceHelper.getKilled(getApplicationContext()));

        Notification notification = getMyActivityNotification(text);

        NotificationManager mNotificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIF_ID, notification);
    }

}
