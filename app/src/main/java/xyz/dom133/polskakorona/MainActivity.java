package xyz.dom133.polskakorona;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.StrictMode;;
import android.provider.Settings;
import android.widget.TextView;

import xyz.dom133.polskakorona.Addons.Constants;
import xyz.dom133.polskakorona.Addons.PreferenceHelper;

public class MainActivity extends AppCompatActivity {

    TextView tv_infected;
    TextView tv_infected_dif;
    TextView tv_infected_hiscore;
    TextView tv_killed;
    TextView tv_killed_up;
    TextView tv_killed_hiscore;
    TextView tv_update_time;

    BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        tv_infected = findViewById(R.id.tv_infected);
        tv_infected_dif = findViewById(R.id.tv_infected_up);
        tv_infected_hiscore = findViewById(R.id.tv_infected_hiscore);
        tv_killed = findViewById(R.id.tv_killed);
        tv_killed_up = findViewById(R.id.tv_killed_up);
        tv_killed_hiscore = findViewById(R.id.tv_killed_hiscore);
        tv_update_time = findViewById(R.id.tv_update_time);

        tv_infected.setText(PreferenceHelper.getInfected(getApplicationContext()));
        tv_infected_dif.setText(PreferenceHelper.getInfectedUp(getApplicationContext()));
        tv_infected_hiscore.setText(PreferenceHelper.getInfectedHiscore(getApplicationContext()));
        tv_killed.setText(PreferenceHelper.getKilled(getApplicationContext()));
        tv_killed_up.setText(PreferenceHelper.getKilledUp(getApplicationContext()));
        tv_killed_hiscore.setText(PreferenceHelper.getKilledHiscore(getApplicationContext()));
        tv_update_time.setText(PreferenceHelper.getUpdateTime(getApplicationContext()));

        createNotificationChannel();

        requestIgnoreBatteryOptimization();

        startUpdateService();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                tv_infected.setText(intent.getStringExtra("infected"));
                tv_infected_dif.setText(intent.getStringExtra("infected_up"));
                tv_infected_hiscore.setText(intent.getStringExtra("infected_hiscore"));
                tv_killed.setText(intent.getStringExtra("killed"));
                tv_killed_up.setText(intent.getStringExtra("killed_up"));
                tv_killed_hiscore.setText(intent.getStringExtra("killed_hiscore"));
                tv_update_time.setText(intent.getStringExtra("update_time"));
            }
        };
    }

    /**
     * Function start update service
     */
    private void startUpdateService() {
        startForegroundService(new Intent(getApplicationContext(), UpdateWorker.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.PACKAGE_NAME);
        registerReceiver(broadcastReceiver, intentFilter);

        tv_infected.setText(PreferenceHelper.getInfected(getApplicationContext()));
        tv_infected_dif.setText(PreferenceHelper.getInfectedUp(getApplicationContext()));
        tv_infected_hiscore.setText(PreferenceHelper.getInfectedHiscore(getApplicationContext()));
        tv_killed.setText(PreferenceHelper.getKilled(getApplicationContext()));
        tv_killed_up.setText(PreferenceHelper.getKilledUp(getApplicationContext()));
        tv_killed_hiscore.setText(PreferenceHelper.getKilledHiscore(getApplicationContext()));
        tv_update_time.setText(PreferenceHelper.getUpdateTime(getApplicationContext()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.PACKAGE_NAME);
        registerReceiver(broadcastReceiver, intentFilter);

        tv_infected.setText(PreferenceHelper.getInfected(getApplicationContext()));
        tv_infected_dif.setText(PreferenceHelper.getInfectedUp(getApplicationContext()));
        tv_infected_hiscore.setText(PreferenceHelper.getInfectedHiscore(getApplicationContext()));
        tv_killed.setText(PreferenceHelper.getKilled(getApplicationContext()));
        tv_killed_up.setText(PreferenceHelper.getKilledUp(getApplicationContext()));
        tv_killed_hiscore.setText(PreferenceHelper.getKilledHiscore(getApplicationContext()));
        tv_update_time.setText(PreferenceHelper.getUpdateTime(getApplicationContext()));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
    }

    /**
     * Function for request battery optimization
     */
    @SuppressLint("BatteryLife")
    private void requestIgnoreBatteryOptimization() {
        PowerManager powerManager = (PowerManager) getApplicationContext().getSystemService(POWER_SERVICE);
        Intent i = new Intent();
        if (!powerManager.isIgnoringBatteryOptimizations(Constants.PACKAGE_NAME)) {
            i.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            i.setData(Uri.parse("package:" + Constants.PACKAGE_NAME));
            startActivity(i);
        }
    }

    /**
     * Function for create notification channels
     */
    private void createNotificationChannel() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "infected_01";
        CharSequence name = getString(R.string.notify_infected_01_channel_name);
        String Description = getString(R.string.notify_infected_01_channel_descryption);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        mChannel.setDescription(Description);
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        mChannel.setShowBadge(false);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);

        CHANNEL_ID = "infected_02";
        name = getString(R.string.notify_infected_02_channel_name);
        Description = getString(R.string.notify_infected_02_channel_descryption);
        importance = NotificationManager.IMPORTANCE_MIN;
        mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        mChannel.setDescription(Description);
        mChannel.setSound(null, null);
        mChannel.enableLights(false);
        mChannel.enableVibration(false);
        mChannel.setShowBadge(false);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(mChannel);
    }

}
